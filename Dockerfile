FROM openjdk:12
ADD target/docker-spring-boot-ricardo_gattino.jar docker-spring-boot-ricardo_gattino.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "docker-spring-boot-ricardo_gattino.jar"]
